FROM php:7.4-apache

RUN mkdir /app
COPY entry-point.sh /app/entry-point.sh
WORKDIR /var/www

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY apache2.conf /etc/apache2/apache2.conf
COPY php.ini /usr/local/etc/php/

RUN set -xe; \
    a2enmod rewrite; \
    apt-get update; \
    apt-get install -y libcurl4-gnutls-dev openssl libssl-dev libpng-dev libreadline-dev libpq-dev gnupg2; \
    \
    docker-php-ext-install bcmath calendar curl dba exif ftp gd hash mbstring mysqli opcache pcntl pdo pdo_mysql sockets zip; \
    yes | pecl install xdebug; \
    echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini; \
    echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini; \
    echo "xdebug.remote_autostart=on" >> /usr/local/etc/php/conf.d/xdebug.ini; \
    \
    curl -sS https://getcomposer.org/installer | php; \
    mv composer.phar /usr/local/bin/composer; \
    \
    curl -sL https://deb.nodesource.com/setup_8.x | bash -; \
    apt-get install nodejs; \
    apt-get clean; \
    npm i -g yarn; \
    ln -s /bin/nodejs /bin/node

CMD ["bash", "/app/entry-point.sh"]
EXPOSE 80/tcp
