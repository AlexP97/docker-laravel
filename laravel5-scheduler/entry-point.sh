#!/usr/bin/env bash

while true
do
    sleep 60
    /usr/local/bin/php /var/www/artisan schedule:run
done
