#!/usr.bin/env bash

set -e

cd /var/www

if [[ ! -f .env ]]
then
    if ! cp .env.example-docker .env 
    then
        cp .env.example .env 
    fi
    composer install
    php artisan key:generate
fi

php artisan migrate

apache2-foreground