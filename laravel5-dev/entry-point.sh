#!/usr/bin/env bash

set -e

cd /var/www

if [[ ! -f .env ]]
then
    if ! cp .env.example-docker .env
    then
        cp .env.example .env
    fi
    composer install
    php artisan key:generate
fi

chown 33:33 -R storage

php artisan migrate || true

apache2-foreground
